DROP DATABASE IF EXISTS testqqminprogram;

CREATE DATABASE IF NOT EXISTS testqqminprogram CHARACTER SET utf8;

USE testqqminprogram;

CREATE TABLE t_cityInfo(
	id INT PRIMARY KEY auto_increment,
	city VARCHAR(30) NOT NULL UNIQUE,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	parent VARCHAR(30),
	updateTime DATETIME
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather0(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather1(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	shidu VARCHAR(30),
	pm25 VARCHAR(30),
	pm10 VARCHAR(30),
	quality VARCHAR(30),
	ganmao VARCHAR(255),
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather2(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather3(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather4(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather5(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather6(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_weather7(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	high VARCHAR(30),
	low VARCHAR(30),
	ymd DATETIME,
	week VARCHAR(30),
	sunrise VARCHAR(30),
	sunset VARCHAR(30),
	aqi VARCHAR(30),
	fx VARCHAR(30),
	fl VARCHAR(30),
	type VARCHAR(30),
	notice VARCHAR(255),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_hour08(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	day VARCHAR(30),
	wea VARCHAR(30),
	tem VARCHAR(30),
	win VARCHAR(30),
	win_speed VARCHAR(30),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_hour11(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	day VARCHAR(30),
	wea VARCHAR(30),
	tem VARCHAR(30),
	win VARCHAR(30),
	win_speed VARCHAR(30),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_hour14(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	day VARCHAR(30),
	wea VARCHAR(30),
	tem VARCHAR(30),
	win VARCHAR(30),
	win_speed VARCHAR(30),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_hour17(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	day VARCHAR(30),
	wea VARCHAR(30),
	tem VARCHAR(30),
	win VARCHAR(30),
	win_speed VARCHAR(30),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_hour20(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	day VARCHAR(30),
	wea VARCHAR(30),
	tem VARCHAR(30),
	win VARCHAR(30),
	win_speed VARCHAR(30),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

CREATE TABLE t_hour23(
	id INT PRIMARY KEY auto_increment,
	cityKey VARCHAR(30) NOT NULL UNIQUE,
	day VARCHAR(30),
	wea VARCHAR(30),
	tem VARCHAR(30),
	win VARCHAR(30),
	win_speed VARCHAR(30),
	FOREIGN KEY (cityKey) REFERENCES t_cityInfo(cityKey)
)ENGINE=INNODB DEFAULT charset=utf8;

-- SELECT * FROM t_cityInfo;
-- SELECT * FROM t_weather0;
-- SELECT * FROM t_weather1;
-- SELECT * FROM t_weather2;
-- SELECT * FROM t_weather3;
-- SELECT * FROM t_weather4;
-- SELECT * FROM t_weather5;
-- SELECT * FROM t_weather6;
-- SELECT * FROM t_weather7;
-- SELECT * FROM t_hour08;
-- SELECT * FROM t_hour11;
-- SELECT * FROM t_hour14;
-- SELECT * FROM t_hour17;
-- SELECT * FROM t_hour20;
-- SELECT * FROM t_hour23;



-- 吴浪SQL部分：
