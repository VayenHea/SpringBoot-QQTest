package com.peaceinstitute.demo.controller.weatherController;

import com.alibaba.fastjson.JSON;
import com.peaceinstitute.demo.pojo.Initialize;
import com.peaceinstitute.demo.config.redis.BloomFilterToRedis;
import com.peaceinstitute.demo.service.weatherService.ISelectInfoService;

import com.peaceinstitute.demo.util.CompareTimeUtil;
import com.peaceinstitute.demo.util.WeatherUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */

@RestController
public class WeatherController {

    @Autowired
    Initialize initialize;
    @Autowired
    ISelectInfoService iSelectInfoService;
    @Autowired
    BloomFilterToRedis bloomRedis;

    @RequestMapping(path = "/WeatherController/selectBasicInfo", produces = "application/json;charset=utf-8")
    public String selectBasicInfo(HttpServletRequest request){

        HashMap<String,Object> result = new HashMap<>();
        try {
            String cityName = request.getParameter("cityName");

            System.out.println(cityName);
//            可近似看做的防止缓存穿透（布隆过滤器），具体实现见com.peaceinstitute.demo.config.filter.MyBloomInterceptor
            String cityKey = WeatherUtil.getCityKey(cityName, initialize.getCityInformation());

            if (cityKey == null){
                throw new Exception("错误/未知的城市！");
            }else {

                long start = System.currentTimeMillis();

                HashMap<String, Object> information = CompareTimeUtil.compareTime(iSelectInfoService.selectBasicInfo(cityKey));

                long end = System.currentTimeMillis();
                System.out.println("本次访问数据库用时:" + (end - start));

                result.put("statusCode",200);
                result.put("information",information);
            }
        }catch (Exception e){
            e.printStackTrace();
            if (e.getMessage().equals("错误/未知的城市！")){
                result.put("statusCode",404);
                result.put("information",e.getMessage());
            }else {
                result.put("statusCode",202);
                result.put("information","服务器繁忙，请稍后再试！");
            }
        }finally {
            System.out.println(result);
            return JSON.toJSONString(result);
        }
    }

    @RequestMapping(path = "/WeatherController/selectMoreInfo", produces = "application/json;charset=utf-8")
    public String selectMoreInfo(HttpServletRequest request){

        HashMap<String,Object> result = new HashMap<>();
        try {
            String cityName = request.getParameter("cityName");
            System.out.println(cityName);
            String cityKey = WeatherUtil.getCityKey(cityName, initialize.getCityInformation());

            if (cityKey == null){
                throw new Exception("错误/未知的城市！");
            }else {

                long start = System.currentTimeMillis();

                HashMap<String, Object> information = CompareTimeUtil.compareTime(iSelectInfoService.selectMoreInfo(cityKey));

                long end = System.currentTimeMillis();
                System.out.println("本次访问数据库用时:" + (end - start));

                result.put("statusCode",200);
                result.put("information",information);
            }
        }catch (Exception e){
            e.printStackTrace();
            if (e.getMessage().equals("错误/未知的城市！")){
                result.put("statusCode",404);
                result.put("information",e.getMessage());
            }else {
                result.put("statusCode",202);
                result.put("information","服务器繁忙，请稍后再试！");
            }
        }finally {
            System.out.println(result);
            return JSON.toJSONString(result);
        }
    }

    @RequestMapping(path = "/WeatherController/selectAllInfo", produces = "application/json;charset=utf-8")
    public String selectAllInfo(HttpServletRequest request){

        HashMap<String,Object> result = new HashMap<>();
        try {
            String cityName = request.getParameter("cityName");
            System.out.println(cityName);
            String cityKey = WeatherUtil.getCityKey(cityName, initialize.getCityInformation());

            if (cityKey == null){
                throw new Exception("错误/未知的城市！");
            }else {

                long start = System.currentTimeMillis();

                HashMap<String, Object> information = CompareTimeUtil.compareTime(iSelectInfoService.selectAllInfo(cityKey));

                long end = System.currentTimeMillis();
                System.out.println("本次访问数据库用时:" + (end - start));

                result.put("statusCode",200);
                result.put("information",information);
            }
        }catch (Exception e){
            e.printStackTrace();
            if (e.getMessage().equals("错误/未知的城市！")){
                result.put("statusCode",404);
                result.put("information",e.getMessage());
            }else {
                result.put("statusCode",202);
                result.put("information","服务器繁忙，请稍后再试！");
            }
        }finally {
            System.out.println(result);
            return JSON.toJSONString(result);
        }
    }
}
