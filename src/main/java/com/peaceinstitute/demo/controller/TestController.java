package com.peaceinstitute.demo.controller;

import com.alibaba.fastjson.JSON;
import com.peaceinstitute.demo.pojo.Initialize;
import com.peaceinstitute.demo.service.weatherService.ISelectInfoService;
import com.peaceinstitute.demo.service.weatherService.impl.Test;
import com.peaceinstitute.demo.util.WeatherUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */

@RestController
public class TestController {

    @Autowired
    Initialize initialize;
    @Autowired
    Test test;
    @Autowired
    ISelectInfoService iSelectInfoService;

    @RequestMapping(path = "/TestController/test", produces = "application/json;charset=utf-8")
    public String test(){
        String information = null;
        HashMap<String,Object> result = new HashMap<>();
        try {
//            long start = System.currentTimeMillis();
//
//            long end = System.currentTimeMillis();
//            System.out.println("本次访问数据库用时:" + (end - start));

            HashMap<String,String> map = WeatherUtil.patternUtil(initialize.getCityInformation());

            String cityKey = map.get("武汉");
            System.out.println("获取到的cityKey：" + cityKey);
            String apiUrl = "http://t.weatherService.sojson.com/api/weatherService/city/".concat(cityKey);
            System.out.println(apiUrl);

            //开始请求
            URL url= new URL(apiUrl);
            URLConnection open = url.openConnection();
            InputStream input = open.getInputStream();
            //这里转换为String，带上包名，避免引错包
            information = org.apache.commons.io.IOUtils.toString(input,"utf-8");
            //输出
            System.out.println(information);

            result.put("statusCode",200);
            result.put("information",information);
        }catch (Exception e){
            result.put("statusCode",202);
            result.put("information","服务器繁忙，请稍后再试！");
        }finally {
            System.out.println(result);
//            return JSON.toJSONString(result);
            return information;
        }
    }

    @RequestMapping(path = "/TestController/get", produces = "application/json;charset=utf-8")
    public String get(){
        System.out.println("666");

        long start = System.currentTimeMillis();

        System.out.println(test.get("101181301"));

        long end = System.currentTimeMillis();

        System.out.println("本次访问数据库用时:" + (end - start));
        return "123";
    }

    @RequestMapping(path = "/TestController/testSelectBasicInfo", produces = "application/json;charset=utf-8")
    public String testSelectBasicInfo(HttpServletRequest request){

        HashMap<String,Object> result = new HashMap<>();
        try {
            String cityName = request.getParameter("cityName");
            String cityKey = WeatherUtil.getCityKey(cityName, initialize.getCityInformation());

            if (cityKey == null){
                throw new Exception("错误/未知的城市！");
            }else {

                long start = System.currentTimeMillis();

                HashMap<String, Object> information = iSelectInfoService.selectBasicInfo(cityKey);

                long end = System.currentTimeMillis();
                System.out.println("本次访问数据库用时:" + (end - start));

                result.put("statusCode",200);
                result.put("information",information);
            }
        }catch (Exception e){
            if (e.getMessage().equals("错误/未知的城市！")){
                result.put("statusCode",404);
                result.put("information",e.getMessage());
            }else {
                result.put("statusCode",202);
                result.put("information","服务器繁忙，请稍后再试！");
            }
        }finally {
            System.out.println(result);
            return JSON.toJSONString(result);
        }
    }

    @RequestMapping(path = "/TestController/testSelectMoreInfo", produces = "application/json;charset=utf-8")
    public String testSelectMoreInfo(HttpServletRequest request){

        HashMap<String,Object> result = new HashMap<>();
        try {
            String cityName = request.getParameter("cityName");
            String cityKey = WeatherUtil.getCityKey(cityName, initialize.getCityInformation());

            if (cityKey == null){
                throw new Exception("错误/未知的城市！");
            }else {

                long start = System.currentTimeMillis();

                HashMap<String, Object> information = iSelectInfoService.selectMoreInfo(cityKey);

                long end = System.currentTimeMillis();
                System.out.println("本次访问数据库用时:" + (end - start));

                result.put("statusCode",200);
                result.put("information",information);
            }
        }catch (Exception e){
            if (e.getMessage().equals("错误/未知的城市！")){
                result.put("statusCode",404);
                result.put("information",e.getMessage());
            }else {
                result.put("statusCode",202);
                result.put("information","服务器繁忙，请稍后再试！");
            }
        }finally {
            System.out.println(result);
            return JSON.toJSONString(result);
        }
    }

    @RequestMapping(path = "/TestController/testSelectAllInfo", produces = "application/json;charset=utf-8")
    public String testSelectAllInfo(HttpServletRequest request){

        HashMap<String,Object> result = new HashMap<>();
        try {
            String cityName = request.getParameter("cityName");
            String cityKey = WeatherUtil.getCityKey(cityName, initialize.getCityInformation());

            if (cityKey == null){
                throw new Exception("错误/未知的城市！");
            }else {

                long start = System.currentTimeMillis();

                HashMap<String, Object> information = iSelectInfoService.selectAllInfo(cityKey);

                long end = System.currentTimeMillis();
                System.out.println("本次访问数据库用时:" + (end - start));

                result.put("statusCode",200);
                result.put("information",information);
            }
        }catch (Exception e){
            if (e.getMessage().equals("错误/未知的城市！")){
                result.put("statusCode",404);
                result.put("information",e.getMessage());
            }else {
                result.put("statusCode",202);
                result.put("information","服务器繁忙，请稍后再试！");
            }
        }finally {
            System.out.println(result);
            return JSON.toJSONString(result);
        }
    }
}
