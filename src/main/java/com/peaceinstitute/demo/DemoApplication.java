package com.peaceinstitute.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//@ImportResource(locations = {"classpath:mybatis-config.xml"})
//@MapperScan(basePackages = "com.peaceinstitute.demo.dao")
@MapperScan(basePackages ="com.peaceinstitute.demo.dao.dataSource1", sqlSessionTemplateRef  = "ds1SqlSessionTemplate")
@MapperScan(basePackages ="com.peaceinstitute.demo.dao.dataSource2", sqlSessionTemplateRef  = "ds2SqlSessionTemplate")
@EnableScheduling
@EnableAsync
@EnableCaching
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = {"com.*"})
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
