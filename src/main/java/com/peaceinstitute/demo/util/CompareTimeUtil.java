package com.peaceinstitute.demo.util;

import com.peaceinstitute.demo.pojo.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */

public class CompareTimeUtil {

    static int startHour = 6;
    static int startMinute = 10;
    static int endHour = 18;
    static int endMinute = 15;


    public static HashMap<String, Object> compareTime(HashMap<String, Object> hashMap) throws ParseException {

        int nowHour = TimeUtil.getNowHour();
        int nowMinute = TimeUtil.getNowMinute();

        ArrayList<Object> arrayList = (ArrayList<Object>) hashMap.get("weather");
        ArrayList<Hours> hoursArrayList = (ArrayList<Hours>) hashMap.get("hours");

        System.out.println(arrayList);
        System.out.println(hoursArrayList);

        if (arrayList != null){
            for (int i = 0; i < arrayList.size(); i++){
                Object object = arrayList.get(i);
                if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather0")){
                    arrayList.set(i, compareTimeWeather0((Weather0) object, nowHour, nowMinute));
                }else if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather1")){
                    arrayList.set(i, compareTimeWeather1((Weather1) object, nowHour, nowMinute));
                }else if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather2")){
                    arrayList.set(i, compareTimeWeather2((Weather2) object, nowHour, nowMinute));
                }else if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather3")){
                    arrayList.set(i, compareTimeWeather3((Weather3) object, nowHour, nowMinute));
                }else if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather4")){
                    arrayList.set(i, compareTimeWeather4((Weather4) object, nowHour, nowMinute));
                }else if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather5")){
                    arrayList.set(i, compareTimeWeather5((Weather5) object, nowHour, nowMinute));
                }else if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather6")){
                    arrayList.set(i, compareTimeWeather6((Weather6) object, nowHour, nowMinute));
                }else if (object.getClass().getName().equals("com.peaceinstitute.demo.pojo.Weather7")){
                    arrayList.set(i, compareTimeWeather7((Weather7) object, nowHour, nowMinute));
                }
            }
            hashMap.put("weather", arrayList);
        }

        if (hoursArrayList != null){
            for (int i = 0; i < hoursArrayList.size(); i++){
                hoursArrayList.set(i, compareTimeHours(hoursArrayList.get(i), nowHour, nowMinute));
            }
            hashMap.put("hours", hoursArrayList);
        }

        return hashMap;
    }

    public static Weather0 compareTimeWeather0(Weather0 weather0, int nowHour, int nowMinute) {
        if (weather0.getType().equals("晴") || weather0.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather0.getType().equals("晴")){
                    weather0.setType("日晴");
                }else {
                    weather0.setType("日间多云");
                }
            }else {
                if (weather0.getType().equals("晴")){
                    weather0.setType("夜晴");
                }else {
                    weather0.setType("夜间多云");
                }
            }
            return weather0;
        }else {
            return weather0;
        }
    }

    public static Weather1 compareTimeWeather1(Weather1 weather1, int nowHour, int nowMinute) {
        if (weather1.getType().equals("晴") || weather1.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather1.getType().equals("晴")){
                    weather1.setType("日晴");
                }else {
                    weather1.setType("日间多云");
                }
            }else {
                if (weather1.getType().equals("晴")){
                    weather1.setType("夜晴");
                }else {
                    weather1.setType("夜间多云");
                }
            }
            return weather1;
        }else {
            return weather1;
        }
    }

    public static Weather2 compareTimeWeather2(Weather2 weather2, int nowHour, int nowMinute) {
        if (weather2.getType().equals("晴") || weather2.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather2.getType().equals("晴")){
                    weather2.setType("日晴");
                }else {
                    weather2.setType("日间多云");
                }
            }else {
                if (weather2.getType().equals("晴")){
                    weather2.setType("夜晴");
                }else {
                    weather2.setType("夜间多云");
                }
            }
            return weather2;
        }else {
            return weather2;
        }
    }

    public static Weather3 compareTimeWeather3(Weather3 weather3, int nowHour, int nowMinute) {
        if (weather3.getType().equals("晴") || weather3.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather3.getType().equals("晴")){
                    weather3.setType("日晴");
                }else {
                    weather3.setType("日间多云");
                }
            }else {
                if (weather3.getType().equals("晴")){
                    weather3.setType("夜晴");
                }else {
                    weather3.setType("夜间多云");
                }
            }
            return weather3;
        }else {
            return weather3;
        }
    }

    public static Weather4 compareTimeWeather4(Weather4 weather4, int nowHour, int nowMinute) {
        if (weather4.getType().equals("晴") || weather4.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather4.getType().equals("晴")){
                    weather4.setType("日晴");
                }else {
                    weather4.setType("日间多云");
                }
            }else {
                if (weather4.getType().equals("晴")){
                    weather4.setType("夜晴");
                }else {
                    weather4.setType("夜间多云");
                }
            }
            return weather4;
        }else {
            return weather4;
        }
    }

    public static Weather5 compareTimeWeather5(Weather5 weather5, int nowHour, int nowMinute) {
        if (weather5.getType().equals("晴") || weather5.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather5.getType().equals("晴")){
                    weather5.setType("日晴");
                }else {
                    weather5.setType("日间多云");
                }
            }else {
                if (weather5.getType().equals("晴")){
                    weather5.setType("夜晴");
                }else {
                    weather5.setType("夜间多云");
                }
            }
            return weather5;
        }else {
            return weather5;
        }
    }

    public static Weather6 compareTimeWeather6(Weather6 weather6, int nowHour, int nowMinute) {
        if (weather6.getType().equals("晴") || weather6.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather6.getType().equals("晴")){
                    weather6.setType("日晴");
                }else {
                    weather6.setType("日间多云");
                }
            }else {
                if (weather6.getType().equals("晴")){
                    weather6.setType("夜晴");
                }else {
                    weather6.setType("夜间多云");
                }
            }
            return weather6;
        }else {
            return weather6;
        }
    }

    public static Weather7 compareTimeWeather7(Weather7 weather7, int nowHour, int nowMinute) {
        if (weather7.getType().equals("晴") || weather7.getType().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (weather7.getType().equals("晴")){
                    weather7.setType("日晴");
                }else {
                    weather7.setType("日间多云");
                }
            }else {
                if (weather7.getType().equals("晴")){
                    weather7.setType("夜晴");
                }else {
                    weather7.setType("夜间多云");
                }
            }
            return weather7;
        }else {
            return weather7;
        }
    }

    public static Hours compareTimeHours(Hours hours, int nowHour, int nowMinute) {
        if (hours.getDay() == null){
            return null;
        }
        if (hours.getWea().equals("晴") || hours.getWea().equals("多云")){
            if ((nowHour > startHour && nowHour < endHour) || (nowHour == startHour && nowMinute >= startMinute) || (nowHour == endHour && nowMinute <= endMinute)){
                if (hours.getWea().equals("晴")){
                    hours.setWea("日晴");
                }else {
                    hours.setWea("日间多云");
                }
            }else {
                if (hours.getWea().equals("晴")){
                    hours.setWea("夜晴");
                }else {
                    hours.setWea("夜间多云");
                }
            }
            return hours;
        }else {
            return hours;
        }
    }
}
