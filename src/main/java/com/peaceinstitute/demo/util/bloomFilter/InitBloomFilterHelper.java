package com.peaceinstitute.demo.util.bloomFilter;

import com.google.common.base.Charsets;
import com.google.common.hash.Funnel;
import org.springframework.stereotype.Component;

/**
 * Describe:
 *
 * @author JZY
 */

@Component
public class InitBloomFilterHelper {

//    预计的样本数
    public static int expectedInsertions = 10000;
//    可接受的误差值
    public static double fpp = 0.0001;

    public BloomFilterHelper<String> initBloomFilterHelper() {
        //不设置第三个参数时，误判率默认为0.03
        //进行误判率的设置，自动计算需要几个hash函数。bit数组的长度与size和fpp参数有关
        //过滤器内部会对size进行处理，保证size为2的n次幂。
        return new BloomFilterHelper<>((Funnel<String>) (from, into) -> into.putString(from, Charsets.UTF_8)
                .putString(from, Charsets.UTF_8), expectedInsertions, fpp);
    }
}
