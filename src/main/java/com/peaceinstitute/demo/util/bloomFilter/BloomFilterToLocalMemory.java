package com.peaceinstitute.demo.util.bloomFilter;

import com.google.common.base.Preconditions;
import com.peaceinstitute.demo.pojo.Initialize;
import com.peaceinstitute.demo.util.WeatherUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Describe:
 *
 * @author JZY
 */

@Component
public class BloomFilterToLocalMemory {

//    bits类型的集合
    private BitSet bitSet = new BitSet();

    /**
     * bits类型的集合中放入初始化数据
     */
//    public void initByBloomFilter(){
//
//        HashMap<String,String> map = WeatherUtil.patternUtil(initialize.getCityInformation());
//
//        System.out.println("本地内存-布隆过滤器开始初始化...");
//        long start = System.currentTimeMillis();
//
////        向布隆过滤器中添加相关key
//        for (Map.Entry<String, String> entry : map.entrySet()){
//            bloomFilterToLocalMemory.addByBloomFilter(bloomFilterHelper, entry.getKey());
//        }
//
//        long end = System.currentTimeMillis();
//
//        System.out.println("本地内存-布隆过滤器在" + (end - start) + "ms内初始化完毕！");
//    }

    /**
     * 根据给定的布隆过滤器添加值
     */
    public <T> void addByBloomFilter(BloomFilterHelper<T> bloomFilterHelper, T value) {
        Preconditions.checkArgument(bloomFilterHelper != null, "bloomFilterHelper不能为空");
        int[] offset = bloomFilterHelper.murmurHashOffset(value);
        for (int i : offset) {
            bitSet.set(i);
        }
    }

    /**
     * 根据给定的布隆过滤器判断值是否存在
     */
    public <T> boolean includeByBloomFilter(BloomFilterHelper<T> bloomFilterHelper, T value) {
        Preconditions.checkArgument(bloomFilterHelper != null, "bloomFilterHelper不能为空");
        int[] offset = bloomFilterHelper.murmurHashOffset(value);
        for (int i : offset) {
            if (!bitSet.get(i)) {
                return false;
            }
        }

        return true;
    }
}
