package com.peaceinstitute.demo.util;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Describe:
 *
 * @author JZY
 */

public class TimeUtil {

    public static int getNowMinute() {

        Calendar now = Calendar.getInstance();
        return now.get(Calendar.MINUTE);
    }

    public static int getNowHour() {

        Calendar now = Calendar.getInstance();
        return now.get(Calendar.HOUR_OF_DAY);
    }

    public static boolean getTimeGap(Date date1, long gap1) throws ParseException {

        long gap =  Math.abs(new Date().getTime() - date1.getTime());

        if (gap > gap1){
            return true;
        }else {
            return false;
        }
    }
}
