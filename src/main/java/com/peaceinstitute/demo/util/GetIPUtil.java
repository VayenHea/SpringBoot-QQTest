package com.peaceinstitute.demo.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Describe:
 *
 * @author JZY
 */

public class GetIPUtil {

    public static String getIntranetIP() throws UnknownHostException {

        InetAddress inetAddress = InetAddress.getLocalHost();
        String hostAddress = inetAddress.getHostAddress();
//        String hostName= inetAddress.getHostName();

        return hostAddress;
    }

    public static String getExtranetIP() throws Exception{

        try {
            // 打开连接
            Document doc = Jsoup.connect("http://chaipip.com/").get();
            Elements eles = doc.select("#ip");
            return eles.attr("value");
        }catch (IOException e) {
            e.printStackTrace();
        }

        return InetAddress.getLocalHost().getHostAddress();
    }
}
