package com.peaceinstitute.demo.util;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;

/**
 * Describe:
 *
 * @author JZY
 */

public class SendMailUtil {

    public static void sendQQMail(JavaMailSender javaMailSender, Exception e, int flag) throws Exception {

//        创建一个可附件的复杂消息邮件
        MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage, true);

//        邮件设置
        mimeMessageHelper.setSubject("QQ小程序出错！");
        mimeMessageHelper.setText("@IP：" + GetIPUtil.getExtranetIP() + "<br><b styles='color:red'>" + e.toString() + "</b><br>" + "<b styles='color:blue'>" + flag + "</b>", true);

        mimeMessageHelper.setTo("1359674233@qq.com");
        mimeMessageHelper.setFrom("2164970087@qq.com");

//        上传文件
//        mimeMessageHelper.addAttachment("photo.png", new File("C:\\Users\\lenovo\\Desktop\\QQ图片20200201151502.png"));

        javaMailSender.send(mimeMailMessage);
    }

    public static void sendQQMail(JavaMailSender javaMailSender, String string) throws Exception {

//        创建一个可附件的复杂消息邮件
        MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage, true);

//        邮件设置
        mimeMessageHelper.setSubject("QQ小程序出错！");
        mimeMessageHelper.setText("@IP：" + GetIPUtil.getExtranetIP() + "<br><b styles='color:red'>" + string + "</b>", true);

        mimeMessageHelper.setTo("1359674233@qq.com");
        mimeMessageHelper.setFrom("2164970087@qq.com");

//        上传文件
//        mimeMessageHelper.addAttachment("photo.png", new File("C:\\Users\\lenovo\\Desktop\\QQ图片20200201151502.png"));

        javaMailSender.send(mimeMailMessage);
    }
}
