package com.peaceinstitute.demo.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.peaceinstitute.demo.pojo.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */

public class EncapsulatePojoUtil {

//    根据获取到的数据，封装成若干pojo
    public static HashMap<String, Object> encapsulatePojo(String information, String realTimeInformation) throws Exception {

//        预测
        JSONObject information1 = JSONObject.parseObject(information);
//        实时
        JSONObject realTimeInformation1 = JSONObject.parseObject(realTimeInformation);

        if (information1 == null || realTimeInformation1 == null){
            return null;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (information1.get("time") == null || TimeUtil.getTimeGap(simpleDateFormat.parse((String)information1.get("time")), 1000 * 60 * 60 * 24)){
            if (information1.get("time") != null){
                throw new Exception("天气数据有效性较低！<br>城市id：" + realTimeInformation1.getString("cityid") + "<br>最近更新时间为：" + information1.get("time"));
            }
            return null;
        }

        JSONObject information2_cityInfo = information1.getJSONObject("cityInfo");
        JSONObject information2_data = information1.getJSONObject("data");

        JSONArray realTimeInformation2 = realTimeInformation1.getJSONArray("data");

        if (information2_cityInfo == null || information2_data == null || realTimeInformation2 == null){
            return null;
        }

        JSONObject information3_yesterday = information2_data.getJSONObject("yesterday");
        JSONArray information3_forecast = information2_data.getJSONArray("forecast");

        JSONObject information3_forecast1 = information3_forecast.getJSONObject(0);
        JSONObject information3_forecast2 = information3_forecast.getJSONObject(1);
        JSONObject information3_forecast3 = information3_forecast.getJSONObject(2);
        JSONObject information3_forecast4 = information3_forecast.getJSONObject(3);
        JSONObject information3_forecast5 = information3_forecast.getJSONObject(4);
        JSONObject information3_forecast6 = information3_forecast.getJSONObject(5);
        JSONObject information3_forecast7 = information3_forecast.getJSONObject(6);

        System.out.println(information1);
        System.out.println(information2_cityInfo);
        System.out.println(information2_data);
        System.out.println(information3_yesterday);
        System.out.println(information3_forecast);
        System.out.println(information3_forecast1);
        System.out.println(information3_forecast2);
        System.out.println(information3_forecast3);
        System.out.println(information3_forecast4);
        System.out.println(information3_forecast5);
        System.out.println(information3_forecast6);
        System.out.println(information3_forecast7);

        CityInfo cityInfo = new CityInfo(null, null, (String)information2_cityInfo.get("citykey"), (String)information2_cityInfo.get("parent"), simpleDateFormat.parse((String)information1.get("time")));

        Weather0 weather0 = new Weather0(null, (String)information2_cityInfo.get("citykey"),
                ((String)information3_yesterday.get("high")).substring(3), ((String)information3_yesterday.get("low")).substring(3),
                simpleDateFormat.parse(information3_yesterday.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_yesterday.get("week"), (String)information3_yesterday.get("sunrise"),
                (String)information3_yesterday.get("sunset"), String.valueOf(information3_yesterday.get("aqi")),
                (String)information3_yesterday.get("fx"), (String)information3_yesterday.get("fl"),
                (String)information3_yesterday.get("type"), (String)information3_yesterday.get("notice"));

        Weather1 weather1 = new Weather1(null, (String)information2_cityInfo.get("citykey"),
                (String)information2_data.get("shidu"), String.valueOf(information2_data.get("pm25")),
                String.valueOf(information2_data.get("pm10")), (String)information2_data.get("quality"), (String)information2_data.get("ganmao"),
                ((String)information3_forecast1.get("high")).substring(3), ((String)information3_forecast1.get("low")).substring(3),
                simpleDateFormat.parse(information3_forecast1.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_forecast1.get("week"), (String)information3_forecast1.get("sunrise"),
                (String)information3_forecast1.get("sunset"), String.valueOf(information3_forecast1.get("aqi")),
                (String)information3_forecast1.get("fx"), (String)information3_forecast1.get("fl"),
                (String)information3_forecast1.get("type"), (String)information3_forecast1.get("notice"));

        Weather2 weather2 = new Weather2(null, (String)information2_cityInfo.get("citykey"),
                ((String)information3_forecast2.get("high")).substring(3), ((String)information3_forecast2.get("low")).substring(3),
                simpleDateFormat.parse(information3_forecast2.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_forecast2.get("week"), (String)information3_forecast2.get("sunrise"),
                (String)information3_forecast2.get("sunset"), String.valueOf(information3_forecast2.get("aqi")),
                (String)information3_forecast2.get("fx"), (String)information3_forecast2.get("fl"),
                (String)information3_forecast2.get("type"), (String)information3_forecast2.get("notice"));

        Weather3 weather3 = new Weather3(null, (String)information2_cityInfo.get("citykey"),
                ((String)information3_forecast3.get("high")).substring(3), ((String)information3_forecast3.get("low")).substring(3),
                simpleDateFormat.parse(information3_forecast3.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_forecast3.get("week"), (String)information3_forecast3.get("sunrise"),
                (String)information3_forecast3.get("sunset"), String.valueOf(information3_forecast2.get("aqi")),
                (String)information3_forecast3.get("fx"), (String)information3_forecast3.get("fl"),
                (String)information3_forecast3.get("type"), (String)information3_forecast3.get("notice"));

        Weather4 weather4 = new Weather4(null,(String)information2_cityInfo.get("citykey"),
                ((String)information3_forecast4.get("high")).substring(3), ((String)information3_forecast4.get("low")).substring(3),
                simpleDateFormat.parse(information3_forecast4.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_forecast4.get("week"), (String)information3_forecast4.get("sunrise"),
                (String)information3_forecast4.get("sunset"), String.valueOf(information3_forecast4.get("aqi")),
                (String)information3_forecast4.get("fx"), (String)information3_forecast4.get("fl"),
                (String)information3_forecast4.get("type"), (String)information3_forecast4.get("notice"));

        Weather5 weather5 = new Weather5(null, (String)information2_cityInfo.get("citykey"),
                ((String)information3_forecast5.get("high")).substring(3), ((String)information3_forecast5.get("low")).substring(3),
                simpleDateFormat.parse(information3_forecast5.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_forecast5.get("week"), (String)information3_forecast5.get("sunrise"),
                (String)information3_forecast5.get("sunset"), String.valueOf(information3_forecast5.get("aqi")),
                (String)information3_forecast5.get("fx"), (String)information3_forecast5.get("fl"),
                (String)information3_forecast5.get("type"), (String)information3_forecast5.get("notice"));

        Weather6 weather6 = new Weather6(null, (String)information2_cityInfo.get("citykey"),
                ((String)information3_forecast6.get("high")).substring(3), ((String)information3_forecast6.get("low")).substring(3),
                simpleDateFormat.parse(information3_forecast6.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_forecast6.get("week"), (String)information3_forecast6.get("sunrise"),
                (String)information3_forecast6.get("sunset"), String.valueOf(information3_forecast6.get("aqi")),
                (String)information3_forecast6.get("fx"), (String)information3_forecast6.get("fl"),
                (String)information3_forecast6.get("type"), (String)information3_forecast6.get("notice"));

        Weather7 weather7 = new Weather7(null, (String)information2_cityInfo.get("citykey"),
                ((String)information3_forecast7.get("high")).substring(3), ((String)information3_forecast7.get("low")).substring(3),
                simpleDateFormat.parse(information3_forecast7.get("ymd") + " " + information2_cityInfo.get("updateTime") + ":00"),
                (String)information3_forecast7.get("week"), (String)information3_forecast7.get("sunrise"),
                (String)information3_forecast7.get("sunset"), String.valueOf(information3_forecast7.get("aqi")),
                (String)information3_forecast7.get("fx"), (String)information3_forecast7.get("fl"),
                (String)information3_forecast7.get("type"), (String)information3_forecast7.get("notice"));

        System.out.println("--------------------------------------------------------------------------------------------");

        JSONObject realTimeInformation3 = realTimeInformation2.getJSONObject(0);
        JSONArray realTimeInformation4 = realTimeInformation3.getJSONArray("hours");

        ArrayList<Hours> arrayList = new ArrayList<>();

        if (realTimeInformation4.size() >= 6){
            for (int i = 0; i < realTimeInformation4.size() - 2; i++){
                JSONObject jsonObject = realTimeInformation4.getJSONObject(i);
                System.out.println(jsonObject);
                Hours hours = new Hours(null, realTimeInformation1.getString("cityid"), jsonObject.getString("day").substring(3, 5),
                        jsonObject.getString("wea"), jsonObject.getString("tem"), jsonObject.getString("win"), jsonObject.getString("win_speed"));
                arrayList.add(hours);
            }
        }

        HashMap<String, Object> pojoMap = new HashMap<>();


        pojoMap.put("cityInfo", cityInfo);
        pojoMap.put("weather0", weather0);
        pojoMap.put("weather1", weather1);
        pojoMap.put("weather2", weather2);
        pojoMap.put("weather3", weather3);
        pojoMap.put("weather4", weather4);
        pojoMap.put("weather5", weather5);
        pojoMap.put("weather6", weather6);
        pojoMap.put("weather7", weather7);

        pojoMap.put("hours", arrayList);

        return pojoMap;
    }
}
