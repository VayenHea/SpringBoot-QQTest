package com.peaceinstitute.demo.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Describe:
 *
 * @author JZY
 */

public class WeatherUtil {

//    分割数据
    public static HashMap<String,String> patternUtil(String string){

 //        用于存放分割好的key-value
        HashMap<String,String> map = new HashMap<>();
//        自定义模式匹配器
        Pattern p = Pattern.compile("[\\u4e00-\\u9fa5]+|\\d+");
        Matcher m = p.matcher(string);
        while ( m.find() ) {
            String cityName = m.group();
            m.find();
            String cityCode = m.group();
            map.put(cityName,cityCode);
        }

        return map;
    }

//    封装api请求方法（预测）
    public static String getWeatherInformation(String cityKey) throws IOException {

        try {
            String apiUrl = "http://t.weather.itboy.net/api/weather/city/".concat(cityKey);
            System.out.println("预测api地址：" + apiUrl);

//            开始请求
            URL url= new URL(apiUrl);
            URLConnection open = url.openConnection();
            InputStream input = open.getInputStream();
//            这里转换为String，带上包名，避免引错包
            String information = org.apache.commons.io.IOUtils.toString(input,"utf-8");

            return information;
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

//    封装api请求方法（实时）
    public static String getRealTimeWeatherInformation(String cityKey) throws IOException {

        try {
            String apiUrl1 = "https://www.tianqiapi.com/api?version=v1&appid=54166916&appsecret=kEPMBzi7&cityid=".concat(cityKey);
            String apiUrl2 = "https://www.tianqiapi.com/api?version=v1&appid=26115179&appsecret=5Q9XzEej&cityid=".concat(cityKey);
            // String apiUrl3 = "https://www.tianqiapi.com/api?version=v1&appid=29941136&appsecret=E8TePq09&cityid=".concat(cityKey);

            String apiUrl = null;

//            解决请求源数据的api调用次数较少的限制(多个账号分批次进行请求源数据)
            if (TimeUtil.getNowHour() < 12){
                System.out.println("实时api地址一：" + apiUrl1);
                apiUrl = apiUrl1;
            }else {
                System.out.println("实时api地址二：" + apiUrl2);
                apiUrl = apiUrl2;
            }

//            开始请求
            URL url= new URL(apiUrl);
            URLConnection open = url.openConnection();
            InputStream input = open.getInputStream();
//            这里转换为String，带上包名，避免引错包
            String realTimeInformation = org.apache.commons.io.IOUtils.toString(input,"utf-8");

            return realTimeInformation;
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    public static String getCityKey(String cityName, String cityInformation){

        String rex1 = "市";
        String rex2 = "镇";
        String rex3 = "县";
        String rex4 = "区";
        cityName = cityName.replace(rex1, "");
        cityName = cityName.replace(rex2, "");
        cityName = cityName.replace(rex3, "");
        cityName = cityName.replace(rex4, "");

        HashMap<String,String> map = WeatherUtil.patternUtil(cityInformation);

        return map.get(cityName);
    }

    public static String getCityKey(String cityName){

        String rex1 = "市";
        String rex2 = "镇";
        String rex3 = "县";
        String rex4 = "区";
        cityName = cityName.replace(rex1, "");
        cityName = cityName.replace(rex2, "");
        cityName = cityName.replace(rex3, "");
        cityName = cityName.replace(rex4, "");

        return cityName;
    }
}
