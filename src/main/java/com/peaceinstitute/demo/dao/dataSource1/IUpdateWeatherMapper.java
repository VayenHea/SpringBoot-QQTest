package com.peaceinstitute.demo.dao.dataSource1;

import com.peaceinstitute.demo.pojo.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

/**
 * Describe:
 *
 * @author JZY
 */

@Repository
public interface IUpdateWeatherMapper {

    @Async
    void updateCityInfo(CityInfo cityInfo);

    @Async
    void updateWeather0(Weather0 weather0);

    @Async
    void updateWeather1(Weather1 weather1);

    @Async
    void updateWeather2(Weather2 weather2);

    @Async
    void updateWeather3(Weather3 weather3);

    @Async
    void updateWeather4(Weather4 weather4);

    @Async
    void updateWeather5(Weather5 weather5);

    @Async
    void updateWeather6(Weather6 weather6);

    @Async
    void updateWeather7(Weather7 weather7);

    @Async
    void updateHours(Hours weather7);
}
