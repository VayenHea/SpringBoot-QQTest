package com.peaceinstitute.demo.dao.dataSource1;

import com.peaceinstitute.demo.pojo.*;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

/**
 * Describe:
 *
 * @author JZY
 */

@Repository
public interface ISelectWeatherMapper {

    CityInfo selectCityInfo(String cityKey);

    Weather0 selectWeather0(String cityKey);

    Weather1 selectWeather1(String cityKey);

    Weather2 selectWeather2(String cityKey);

    Weather3 selectWeather3(String cityKey);

    Weather4 selectWeather4(String cityKey);

    Weather5 selectWeather5(String cityKey);

    Weather6 selectWeather6(String cityKey);

    Weather7 selectWeather7(String cityKey);

    Hours selectHour08(String cityKey);

    Hours selectHour11(String cityKey);

    Hours selectHour14(String cityKey);

    Hours selectHour17(String cityKey);

    Hours selectHour20(String cityKey);

    Hours selectHour23(String cityKey);
}
