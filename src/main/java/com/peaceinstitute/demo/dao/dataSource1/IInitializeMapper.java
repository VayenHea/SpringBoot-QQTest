package com.peaceinstitute.demo.dao.dataSource1;

import org.springframework.stereotype.Repository;

/**
 * Describe:
 *
 * @author JZY
 */

@Repository
public interface IInitializeMapper {

    int initializeCityInfo(String city, String cityKey, String parent);

    int initializeWeather0(String cityKey);

    int initializeWeather1(String cityKey);

    int initializeWeather2(String cityKey);

    int initializeWeather3(String cityKey);

    int initializeWeather4(String cityKey);

    int initializeWeather5(String cityKey);

    int initializeWeather6(String cityKey);

    int initializeWeather7(String cityKey);

    int initializeHour08(String cityKey);

    int initializeHour11(String cityKey);

    int initializeHour14(String cityKey);

    int initializeHour17(String cityKey);

    int initializeHour20(String cityKey);

    int initializeHour23(String cityKey);

}
