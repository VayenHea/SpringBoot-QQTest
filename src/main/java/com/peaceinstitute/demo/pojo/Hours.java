package com.peaceinstitute.demo.pojo;

import java.io.Serializable;

public class Hours implements Serializable {
    private Integer id;

    private String citykey;

    private String day;

    private String wea;

    private String tem;

    private String win;

    private String winSpeed;

    private static final long serialVersionUID = 1L;

    public Hours(Integer id, String citykey, String day, String wea, String tem, String win, String winSpeed) {
        this.id = id;
        this.citykey = citykey;
        this.day = day;
        this.wea = wea;
        this.tem = tem;
        this.win = win;
        this.winSpeed = winSpeed;
    }

    public Hours() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCitykey() {
        return citykey;
    }

    public void setCitykey(String citykey) {
        this.citykey = citykey == null ? null : citykey.trim();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day == null ? null : day.trim();
    }

    public String getWea() {
        return wea;
    }

    public void setWea(String wea) {
        this.wea = wea == null ? null : wea.trim();
    }

    public String getTem() {
        return tem;
    }

    public void setTem(String tem) {
        this.tem = tem == null ? null : tem.trim();
    }

    public String getWin() {
        return win;
    }

    public void setWin(String win) {
        this.win = win == null ? null : win.trim();
    }

    public String getWinSpeed() {
        return winSpeed;
    }

    public void setWinSpeed(String winSpeed) {
        this.winSpeed = winSpeed == null ? null : winSpeed.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", citykey=").append(citykey);
        sb.append(", day=").append(day);
        sb.append(", wea=").append(wea);
        sb.append(", tem=").append(tem);
        sb.append(", win=").append(win);
        sb.append(", winSpeed=").append(winSpeed);
        sb.append("]");
        return sb.toString();
    }
}