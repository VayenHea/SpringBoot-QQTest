package com.peaceinstitute.demo.pojo;

import java.io.Serializable;
import java.util.Date;

public class Weather6 implements Serializable {
    private Integer id;

    private String citykey;

    private String high;

    private String low;

    private Date ymd;

    private String week;

    private String sunrise;

    private String sunset;

    private String aqi;

    private String fx;

    private String fl;

    private String type;

    private String notice;

    private static final long serialVersionUID = 1L;

    public Weather6(Integer id, String citykey, String high, String low, Date ymd, String week, String sunrise, String sunset, String aqi, String fx, String fl, String type, String notice) {
        this.id = id;
        this.citykey = citykey;
        this.high = high;
        this.low = low;
        this.ymd = ymd;
        this.week = week;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.aqi = aqi;
        this.fx = fx;
        this.fl = fl;
        this.type = type;
        this.notice = notice;
    }

    public Weather6() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCitykey() {
        return citykey;
    }

    public void setCitykey(String citykey) {
        this.citykey = citykey == null ? null : citykey.trim();
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high == null ? null : high.trim();
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low == null ? null : low.trim();
    }

    public Date getYmd() {
        return ymd;
    }

    public void setYmd(Date ymd) {
        this.ymd = ymd;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week == null ? null : week.trim();
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise == null ? null : sunrise.trim();
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset == null ? null : sunset.trim();
    }

    public String getAqi() {
        return aqi;
    }

    public void setAqi(String aqi) {
        this.aqi = aqi == null ? null : aqi.trim();
    }

    public String getFx() {
        return fx;
    }

    public void setFx(String fx) {
        this.fx = fx == null ? null : fx.trim();
    }

    public String getFl() {
        return fl;
    }

    public void setFl(String fl) {
        this.fl = fl == null ? null : fl.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", citykey=").append(citykey);
        sb.append(", high=").append(high);
        sb.append(", low=").append(low);
        sb.append(", ymd=").append(ymd);
        sb.append(", week=").append(week);
        sb.append(", sunrise=").append(sunrise);
        sb.append(", sunset=").append(sunset);
        sb.append(", aqi=").append(aqi);
        sb.append(", fx=").append(fx);
        sb.append(", fl=").append(fl);
        sb.append(", type=").append(type);
        sb.append(", notice=").append(notice);
        sb.append("]");
        return sb.toString();
    }
}