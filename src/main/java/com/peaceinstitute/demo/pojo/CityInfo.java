package com.peaceinstitute.demo.pojo;

import java.io.Serializable;
import java.util.Date;

public class CityInfo implements Serializable {
    private Integer id;

    private String city;

    private String citykey;

    private String parent;

    private Date updatetime;

    private static final long serialVersionUID = 1L;

    public CityInfo(Integer id, String city, String citykey, String parent, Date updatetime) {
        this.id = id;
        this.city = city;
        this.citykey = citykey;
        this.parent = parent;
        this.updatetime = updatetime;
    }

    public CityInfo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getCitykey() {
        return citykey;
    }

    public void setCitykey(String citykey) {
        this.citykey = citykey == null ? null : citykey.trim();
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent == null ? null : parent.trim();
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", city=").append(city);
        sb.append(", citykey=").append(citykey);
        sb.append(", parent=").append(parent);
        sb.append(", updatetime=").append(updatetime);
        sb.append("]");
        return sb.toString();
    }
}