package com.peaceinstitute.demo.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Describe:
 *
 * @author JZY
 */

@Component
@ConfigurationProperties(prefix = "initialize")
public class Initialize {

    private String cityInformation;

    public String getCityInformation() {
        return cityInformation;
    }

    public void setCityInformation(String cityInformation) {
        this.cityInformation = cityInformation;
    }

    @Override
    public String toString() {
        return "Initialize{" +
                "cityInformation='" + cityInformation + '\'' +
                '}';
    }
}
