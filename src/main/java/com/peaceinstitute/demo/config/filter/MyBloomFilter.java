package com.peaceinstitute.demo.config.filter;

import com.peaceinstitute.demo.util.bloomFilter.BloomFilterToLocalMemory;
import com.peaceinstitute.demo.util.WeatherUtil;
import com.peaceinstitute.demo.util.bloomFilter.InitBloomFilterHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStreamWriter;

/**
 * Describe: 防止缓存穿透（布隆过滤器）
 *
 * @author JZY
 */

@Component
public class MyBloomFilter implements HandlerInterceptor {

//    @Autowired
//    BloomFilterToRedis bloomFilterToRedis;
    @Autowired
    InitBloomFilterHelper initBloomFilterHelper;
    @Autowired
    BloomFilterToLocalMemory bloomFilterToLocalMemory;

    /**
     * 预处理回调方法，实现处理器的预处理，进入controller层之前拦截请求
     * @param request
     * @param response
     * @param handler
     * @return 返回值：true表示继续流程；false表示流程中断，不会继续调用其他的拦截器或处理器
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        long start = System.currentTimeMillis();

//        调用已经预先配置好的布隆过滤器
//        redis级别的布隆过滤器
//        Boolean b = bloomFilterToRedis.includeByBloomFilter(bloomFilterHelper, "BLOOM:Weather", WeatherUtil.getCityKey(request.getParameter("cityName")));
//        本地内存级别的布隆过滤器
        Boolean b = bloomFilterToLocalMemory.includeByBloomFilter(initBloomFilterHelper.initBloomFilterHelper(), WeatherUtil.getCityKey(request.getParameter("cityName")));

        long end = System.currentTimeMillis();
        System.out.println("用时:" + (end - start) + "    本次布隆过滤器校验结果：" + b);

//        如果根据布隆过滤器中的算法判定不属于合法的key，则拦截次请求，禁止其进入下一步
        if (!b){

//            推荐状态码202（服务器已接受请求，但尚未处理。正如它可能被拒绝一样，最终该请求可能会也可能不会被执行。）
            response.setStatus(202);
            response.sendError(202);
            ServletOutputStream out = response.getOutputStream();
            OutputStreamWriter ow = new OutputStreamWriter(out,"UTF-8");
            ow.write("非法请求（本次请求参数不合法，已经被拦截）！");
            ow.flush();
            ow.close();

            System.out.println("本次请求已经被拦截！");
            return false;
        }
        return true;
    }

    /**
     * 后处理回调方法，实现处理器（controller）的后处理，但在渲染视图之前
     * 可以通过modelAndView对模型数据进行处理或对视图进行处理
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * 整个请求处理完毕回调方法，即在视图渲染完毕时回调，
     * 如性能监控中我们可以在此记录结束时间并输出消耗时间，
     * 还可以进行一些资源清理，类似于try-catch-finally中的finally，
     * 但仅调用处理器执行链中
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
