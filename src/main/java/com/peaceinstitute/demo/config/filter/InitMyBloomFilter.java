package com.peaceinstitute.demo.config.filter;

import com.peaceinstitute.demo.pojo.Initialize;
import com.peaceinstitute.demo.util.WeatherUtil;
import com.peaceinstitute.demo.util.bloomFilter.BloomFilterToLocalMemory;
import com.peaceinstitute.demo.util.bloomFilter.InitBloomFilterHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Describe:
 *
 * @author JZY
 */

@Component
public class InitMyBloomFilter {

    @Autowired
    Initialize initialize;
    @Autowired
    InitBloomFilterHelper initBloomFilterHelper;
    @Autowired
    BloomFilterToLocalMemory bloomFilterToLocalMemory;

    public void initMyBloomFilter(){

        HashMap<String,String> map = WeatherUtil.patternUtil(initialize.getCityInformation());

        System.out.println("本地内存-布隆过滤器开始初始化...");
        long start = System.currentTimeMillis();

//        向布隆过滤器中添加相关key
        for (Map.Entry<String, String> entry : map.entrySet()){
            bloomFilterToLocalMemory.addByBloomFilter(initBloomFilterHelper.initBloomFilterHelper(), entry.getKey());
        }

        long end = System.currentTimeMillis();

        System.out.println("本地内存-布隆过滤器在" + (end - start) + "ms内初始化完毕！");
    }
}
