package com.peaceinstitute.demo.config.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Describe:
 *
 * @author JZY
 */

@Configuration
public class FilterConfig implements WebMvcConfigurer {

    @Autowired
    MyBloomFilter myBloomFilter;
    @Autowired
    InitMyBloomFilter initMyBloomFilter;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        initMyBloomFilter.initMyBloomFilter();

//        装载自定义的拦截器
        registry.addInterceptor(myBloomFilter)
//                添加拦截路径
                .addPathPatterns("/WeatherController/**");
//                排除拦截路径
//                .excludePathPatterns("/static/css/**")
//                .excludePathPatterns("/static/images/**")
//                .excludePathPatterns("/static/js/**");
    }
}
