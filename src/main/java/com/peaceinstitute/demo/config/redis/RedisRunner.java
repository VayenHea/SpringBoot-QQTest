//package com.peaceinstitute.demo.config.redis;
//
//import com.peaceinstitute.demo.pojo.Initialize;
//import com.peaceinstitute.demo.util.bloomFilter.BloomFilterHelper;
//import com.peaceinstitute.demo.util.WeatherUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Component;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Describe: RedisRunner-用于在项目启动时加载需要的redis相关内容
// *
// * @author JZY
// */
//
//@Component
//public class RedisRunner implements CommandLineRunner {
//
//    @Autowired
//    Initialize initialize;
//    @Autowired
//    private BloomFilterToRedis bloomFilterToRedis;
//    @Autowired
//    private BloomFilterHelper bloomFilterHelper;
//    @Autowired
//    private RedisTemplate redisTemplate;
//
//    @Override
//    public void run(String... args) throws Exception {
//
////        服务器重启时删除之前向布隆过滤器中添加相关key内容（可选）
//        redisTemplate.delete("BLOOM:Weather");
//
//        HashMap<String,String> map = WeatherUtil.patternUtil(initialize.getCityInformation());
//
//        System.out.println("Redis-布隆过滤器开始初始化...");
//        long start = System.currentTimeMillis();
//
////        向布隆过滤器中添加相关key
//        for (Map.Entry<String, String> entry : map.entrySet()){
//            bloomFilterToRedis.addByBloomFilter(bloomFilterHelper, "BLOOM:Weather", entry.getKey());
//        }
//
//        long end = System.currentTimeMillis();
//        System.out.println("Redis-布隆过滤器在" + (end - start) + "ms内初始化完毕！");
//    }
//}