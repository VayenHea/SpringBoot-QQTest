package com.peaceinstitute.demo.config.dataSource;

// 修改MyBatis的自动配置（也可通过配置文件进行开启）
// 例如开启驼峰命名法等
//@org.springframework.context.annotation.Configuration
//public class MyBatisConfig {
//
//    @Bean
//    public ConfigurationCustomizer configurationCustomizer(){
//        return new ConfigurationCustomizer(){
//
//            @Override
//            public void customize(Configuration configuration) {
//                configuration.setMapUnderscoreToCamelCase(true);
//            }
//        };
//    }
//}
