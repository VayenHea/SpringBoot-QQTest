package com.peaceinstitute.demo.service.weatherService.impl;

import com.peaceinstitute.demo.dao.dataSource1.ISelectWeatherMapper;
import com.peaceinstitute.demo.pojo.*;
import com.peaceinstitute.demo.service.weatherService.ISelectWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import org.springframework.stereotype.Service;

/**
 * Describe:
 *
 * @author JZY
 */

@Service
public class ISelectWeatherServiceImpl implements ISelectWeatherService {

    @Autowired
    ISelectWeatherMapper iSelectWeatherMapper;

    @Cacheable(cacheNames = "cityInfo", key = "#cityKey")
    @Override
    public CityInfo selectCityInfo(String cityKey) {
        return iSelectWeatherMapper.selectCityInfo(cityKey);
    }

    @Cacheable(cacheNames = "weather0", key = "#cityKey")
    @Override
    public Weather0 selectWeather0(String cityKey) {
        return iSelectWeatherMapper.selectWeather0(cityKey);
    }

    @Cacheable(cacheNames = "weather1", key = "#cityKey")
    @Override
    public Weather1 selectWeather1(String cityKey) {
        return iSelectWeatherMapper.selectWeather1(cityKey);
    }

    @Cacheable(cacheNames = "weather2", key = "#cityKey")
    @Override
    public Weather2 selectWeather2(String cityKey) {
        return iSelectWeatherMapper.selectWeather2(cityKey);
    }

    @Cacheable(cacheNames = "weather3", key = "#cityKey")
    @Override
    public Weather3 selectWeather3(String cityKey) {
        return iSelectWeatherMapper.selectWeather3(cityKey);
    }

    @Cacheable(cacheNames = "weather4", key = "#cityKey")
    @Override
    public Weather4 selectWeather4(String cityKey) {
        return iSelectWeatherMapper.selectWeather4(cityKey);
    }

    @Cacheable(cacheNames = "weather5", key = "#cityKey")
    @Override
    public Weather5 selectWeather5(String cityKey) {
        return iSelectWeatherMapper.selectWeather5(cityKey);
    }

    @Cacheable(cacheNames = "weather6", key = "#cityKey")
    @Override
    public Weather6 selectWeather6(String cityKey) {
        return iSelectWeatherMapper.selectWeather6(cityKey);
    }

    @Cacheable(cacheNames = "weather7", key = "#cityKey")
    @Override
    public Weather7 selectWeather7(String cityKey) {
        return iSelectWeatherMapper.selectWeather7(cityKey);
    }

    @Cacheable(cacheNames = "hour08", key = "#cityKey")
    @Override
    public Hours selectHour08(String cityKey) {
        return iSelectWeatherMapper.selectHour08(cityKey);
    }

    @Cacheable(cacheNames = "hour11", key = "#cityKey")
    @Override
    public Hours selectHour11(String cityKey) {
        return iSelectWeatherMapper.selectHour11(cityKey);
    }

    @Cacheable(cacheNames = "hour14", key = "#cityKey")
    @Override
    public Hours selectHour14(String cityKey) {
        return iSelectWeatherMapper.selectHour14(cityKey);
    }

    @Cacheable(cacheNames = "hour17", key = "#cityKey")
    @Override
    public Hours selectHour17(String cityKey) {
        return iSelectWeatherMapper.selectHour17(cityKey);
    }

    @Cacheable(cacheNames = "hour20", key = "#cityKey")
    @Override
    public Hours selectHour20(String cityKey) {
        return iSelectWeatherMapper.selectHour20(cityKey);
    }

    @Cacheable(cacheNames = "hour23", key = "#cityKey")
    @Override
    public Hours selectHour23(String cityKey) {
        return iSelectWeatherMapper.selectHour23(cityKey);
    }
}
