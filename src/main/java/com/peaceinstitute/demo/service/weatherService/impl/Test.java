package com.peaceinstitute.demo.service.weatherService.impl;

import com.peaceinstitute.demo.pojo.Weather1;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Describe:
 *
 * @author JZY
 */

@Service
public class Test {

    @Cacheable(cacheNames = "weather1", key = "#cityKey")
    public Weather1 get(String cityKey){
        System.out.println("没查Redis缓存");
        return null;
    }
}
