package com.peaceinstitute.demo.service.weatherService;

import java.io.IOException;
import java.text.ParseException;

/**
 * Describe:
 *
 * @author JZY
 */

public interface ITimingTaskService {

    void updateAll() throws Exception;
}
