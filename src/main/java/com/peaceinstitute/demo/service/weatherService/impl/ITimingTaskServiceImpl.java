package com.peaceinstitute.demo.service.weatherService.impl;

import com.peaceinstitute.demo.pojo.*;
import com.peaceinstitute.demo.service.weatherService.ITimingTaskService;
import com.peaceinstitute.demo.service.weatherService.IUpdateWeatherService;
import com.peaceinstitute.demo.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Describe:
 *
 * @author JZY
 */

@Service
public class ITimingTaskServiceImpl implements ITimingTaskService {

    @Autowired
    Initialize initialize;
    @Autowired
    IUpdateWeatherService iUpdateWeatherService;
    @Autowired
    JavaMailSender javaMailSender;

   // 用于累加出错次数，较少次重新执行方法
    int sum = 0;
   // 标记更新上次出错位置
    int flag = 0;
   // “java_net_NoRouteToHostException”异常相关信息收集
    int java_net_NoRouteToHostException = 0;
    String java_net_NoRouteToHostException_information = "";

   // SpringBoot的定时任务
   // 每天的8点半，10点半、14点半和16点半执行一次
   @Scheduled(cron = "0 30 8,10,14,16 * * ?")
   // @Scheduled(cron = "0 30 11 * * ?")
   // 因其定时任务的性质以及调用的api接口访问的频繁次数要求可近似防止缓存雪崩
    @Override
    public void updateAll() throws Exception {

        HashMap<String,String> map = WeatherUtil.patternUtil(initialize.getCityInformation());

        int i = 1;

        try {

            for (Map.Entry<String, String> entry : map.entrySet()){

                if (i < flag){
                    i++;
                    continue;
                }

                System.out.println(i);

//                通过工具类发送api请求，获得数据
                String information = WeatherUtil.getWeatherInformation(entry.getValue());
                String realTimeInformation = WeatherUtil.getRealTimeWeatherInformation(entry.getValue());

//                通过工具类获取实体集
                HashMap<String, Object> pojoMap = null;
                try {
                    pojoMap = EncapsulatePojoUtil.encapsulatePojo(information, realTimeInformation);
                }catch (Exception e){
                    e.printStackTrace();
                    if (e.getMessage().contains("天气数据有效性较低！")){
                        SendMailUtil.sendQQMail(javaMailSender, e, i);
                        i++;
                        sum = 0;
                    }else {
                        throw e;
                    }
                }

                if (pojoMap == null){
                    Thread.sleep(10000);
//                    无法获取到相应的实体集（可能api请求参数异常/对应api服务器无响应），跳过该城市
//                    SendMailUtil.sendQQMail(javaMailSender, "无法获得编号：" + --i + "的地区天气！");
                    i++;
                    sum = 0;
                    continue;
                }

//                调用封装的更新方法
                iUpdateWeatherService.updateAll(pojoMap, ((CityInfo)pojoMap.get("cityInfo")).getCitykey());

//                if (i == 3){
//                    int k = 10/0;
//                }

//                防止ip被封（通过工具类处理）
                Thread.sleep(RandomNumberUtil.getRandomNumber(5000,10000));

//                再度暂停线程，确保不被封ip
                Thread.sleep(5000);

                i++;
                sum = 0;
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("出错了！");
            try {
//                SpringBoot的邮件任务
//                如果出现异常，将相关信息通知管理员
                if (e.getClass().getName().equals("java.net.NoRouteToHostException")){
//                    待解决异常
                    java_net_NoRouteToHostException++;
                    java_net_NoRouteToHostException_information = java_net_NoRouteToHostException_information + "<br>" + i;
                }else {
                    sum++;
                    SendMailUtil.sendQQMail(javaMailSender, e, i);
                }
            } catch (MessagingException e1) {
                e1.printStackTrace();
            }

//            重新执行
            if (sum < 2){
                flag = i;
                Thread.sleep(5000);
                updateAll();
            }else {
                if (sum >= 2){
                    try {
                        SendMailUtil.sendQQMail(javaMailSender, "累加出错次数已满：sum = " + sum + "！请及时查看源码调试！");
                    } catch (MessagingException e1) {
                        e1.printStackTrace();
                    }
                }
            }

//            throw e;

        }finally {
            if (java_net_NoRouteToHostException != 0){
                try {
                    SendMailUtil.sendQQMail(javaMailSender, "本次更新出现“java.net.NoRouteToHostException”异常次数："+ java_net_NoRouteToHostException + "次<br>" + java_net_NoRouteToHostException_information);
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!(e.getClass().getName().equals("javax.mail.MessagingException"))){
                        try {
                            SendMailUtil.sendQQMail(javaMailSender, e.toString());
                        } catch (MessagingException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                java_net_NoRouteToHostException = 0;
            }
            System.out.println("本次更新结束!");
        }
    }

    @Scheduled(cron = "0 25 8,10,14,16 * * ?")
    public void updateSum() {
        sum = 0;
        flag = 0;
        java_net_NoRouteToHostException = 0;
        java_net_NoRouteToHostException_information = "";
        System.out.println("次数清零完成！");
    }
}
