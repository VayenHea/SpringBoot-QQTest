package com.peaceinstitute.demo.service.weatherService.impl;

import com.peaceinstitute.demo.dao.dataSource1.IUpdateWeatherMapper;
import com.peaceinstitute.demo.pojo.*;
import com.peaceinstitute.demo.service.weatherService.IUpdateWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */

@Service
public class IUpdateWeatherWeatherServiceImpl implements IUpdateWeatherService {

    @Autowired
    IUpdateWeatherMapper iUpdateWeatherMapper;
    @Autowired
    StringRedisTemplate stringRedisTemplate;  //操作k-v都是字符串的
    @Autowired
    RedisTemplate redisTemplate;  //k-v都是对象的

//    定点清除一些整体集合缓存
    @CacheEvict(cacheNames = {"basicInfo", "moreInfo", "allInfo"}, key = "#cityKey")
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAll(HashMap<String, Object> pojoMap, String cityKey) {

//        调用具体的更新方法
        try {
            updateCityInfo((CityInfo) pojoMap.get("cityInfo"));
            updateWeather0((Weather0) pojoMap.get("weather0"));
            updateWeather1((Weather1) pojoMap.get("weather1"));
            updateWeather2((Weather2) pojoMap.get("weather2"));
            updateWeather3((Weather3) pojoMap.get("weather3"));
            updateWeather4((Weather4) pojoMap.get("weather4"));
            updateWeather5((Weather5) pojoMap.get("weather5"));
            updateWeather6((Weather6) pojoMap.get("weather6"));
            updateWeather7((Weather7) pojoMap.get("weather7"));
            updateHours((ArrayList<Hours>) pojoMap.get("hours"));
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateCityInfo(CityInfo cityInfo) {

        try {
            iUpdateWeatherMapper.updateCityInfo(cityInfo);
            redisTemplate.opsForValue().set("cityInfo::" + cityInfo.getCitykey(), cityInfo);
        }catch (Exception e){
            throw e;
        }
    }

//    SpringBoot的异步任务
    @Transactional
    @Async
    @Override
    public void updateWeather0(Weather0 weather0) {

        try {
            iUpdateWeatherMapper.updateWeather0(weather0);
            redisTemplate.opsForValue().set("weather0::" + weather0.getCitykey(), weather0);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateWeather1(Weather1 weather1) {

        try {
            iUpdateWeatherMapper.updateWeather1(weather1);
            redisTemplate.opsForValue().set("weather1::" + weather1.getCitykey(), weather1);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateWeather2(Weather2 weather2) {

        try {
            iUpdateWeatherMapper.updateWeather2(weather2);
            redisTemplate.opsForValue().set("weather2::" + weather2.getCitykey(), weather2);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateWeather3(Weather3 weather3) {

        try {
            iUpdateWeatherMapper.updateWeather3(weather3);
            redisTemplate.opsForValue().set("weather3::" + weather3.getCitykey(), weather3);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateWeather4(Weather4 weather4) {

        try {
            iUpdateWeatherMapper.updateWeather4(weather4);
            redisTemplate.opsForValue().set("weather4::" + weather4.getCitykey(), weather4);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateWeather5(Weather5 weather5) {

        try {
            iUpdateWeatherMapper.updateWeather5(weather5);
            redisTemplate.opsForValue().set("weather5::" + weather5.getCitykey(), weather5);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateWeather6(Weather6 weather6) {

        try {
            iUpdateWeatherMapper.updateWeather6(weather6);
            redisTemplate.opsForValue().set("weather6::" + weather6.getCitykey(), weather6);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateWeather7(Weather7 weather7) {

        try {
            iUpdateWeatherMapper.updateWeather7(weather7);
            redisTemplate.opsForValue().set("weather7::" + weather7.getCitykey(), weather7);
        }catch (Exception e){
            throw e;
        }
    }

    @Transactional
    @Async
    @Override
    public void updateHours(ArrayList<Hours> arrayList) {

        try {
            for (int i = 0; i < arrayList.size(); i++){
                Hours hours = arrayList.get(i);
                System.out.println(hours.getDay());
                iUpdateWeatherMapper.updateHours(hours);
                redisTemplate.opsForValue().set("hour" + hours.getDay() + "::" + hours.getCitykey(), hours);
            }
        }catch (Exception e){
            throw e;
        }
    }
}
