package com.peaceinstitute.demo.service.weatherService;

import java.text.ParseException;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */
public interface ISelectInfoService {

    HashMap<String, Object> selectBasicInfo(String cityKey) throws ParseException;

    HashMap<String, Object> selectMoreInfo(String cityKey) throws ParseException;

    HashMap<String, Object> selectAllInfo(String cityKey) throws ParseException;
}
