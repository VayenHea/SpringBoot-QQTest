package com.peaceinstitute.demo.service.weatherService.impl;

import com.peaceinstitute.demo.service.weatherService.ISelectInfoService;
import com.peaceinstitute.demo.service.weatherService.ISelectWeatherService;
import com.peaceinstitute.demo.util.ArraryListUtil;
import com.peaceinstitute.demo.util.CompareTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */

@Service
public class ISelectInfoServiceImpl implements ISelectInfoService {

    @Autowired
    ISelectWeatherService iSelectWeatherService;

    @Cacheable(cacheNames = "basicInfo", key = "#cityKey")
    @Override
    public HashMap<String, Object> selectBasicInfo(String cityKey) throws ParseException {

        HashMap<String, Object> information = new HashMap<>();
        ArrayList<Object> weather = new ArrayList<>();
        ArrayList<Object> hours = new ArrayList<>();

        weather.add(iSelectWeatherService.selectWeather1(cityKey));
        weather.add(iSelectWeatherService.selectWeather2(cityKey));
        weather.add(iSelectWeatherService.selectWeather3(cityKey));
        weather.add(iSelectWeatherService.selectWeather4(cityKey));

        hours.add(iSelectWeatherService.selectHour08(cityKey));
        hours.add(iSelectWeatherService.selectHour11(cityKey));
        hours.add(iSelectWeatherService.selectHour14(cityKey));
        hours.add(iSelectWeatherService.selectHour17(cityKey));
        hours.add(iSelectWeatherService.selectHour20(cityKey));
        hours.add(iSelectWeatherService.selectHour23(cityKey));

        information.put("cityInfo", iSelectWeatherService.selectCityInfo(cityKey));
        information.put("weather", weather);
        information.put("hours", ArraryListUtil.arraryListToEmpty(hours));

        return information;
    }

    @Cacheable(cacheNames = "moreInfo", key = "#cityKey")
    @Override
    public HashMap<String, Object> selectMoreInfo(String cityKey) throws ParseException {

        HashMap<String, Object> information = new HashMap<>();
        ArrayList<Object> weather = new ArrayList<>();

        weather.add(iSelectWeatherService.selectWeather0(cityKey));
        weather.add(iSelectWeatherService.selectWeather5(cityKey));
        weather.add(iSelectWeatherService.selectWeather6(cityKey));
        weather.add(iSelectWeatherService.selectWeather7(cityKey));

        information.put("weather", weather);

        return information;
    }

    @Cacheable(cacheNames = "allInfo", key = "#cityKey")
    @Override
    public HashMap<String, Object> selectAllInfo(String cityKey) throws ParseException {

        HashMap<String, Object> information = new HashMap<>();
        ArrayList<Object> weather = new ArrayList<>();

        weather.add(iSelectWeatherService.selectWeather0(cityKey));
        weather.add(iSelectWeatherService.selectWeather1(cityKey));
        weather.add(iSelectWeatherService.selectWeather2(cityKey));
        weather.add(iSelectWeatherService.selectWeather3(cityKey));
        weather.add(iSelectWeatherService.selectWeather4(cityKey));
        weather.add(iSelectWeatherService.selectWeather5(cityKey));
        weather.add(iSelectWeatherService.selectWeather6(cityKey));
        weather.add(iSelectWeatherService.selectWeather7(cityKey));


        information.put("cityInfo", iSelectWeatherService.selectCityInfo(cityKey));
        information.put("weather", weather);

        return information;
    }
}
