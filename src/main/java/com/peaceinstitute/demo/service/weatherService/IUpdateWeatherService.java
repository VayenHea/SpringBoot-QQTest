package com.peaceinstitute.demo.service.weatherService;

import com.peaceinstitute.demo.pojo.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Describe:
 *
 * @author JZY
 */

public interface IUpdateWeatherService {

    void updateAll(HashMap<String, Object> pojoMap, String cityKey);

    void updateCityInfo(CityInfo cityInfo);

    void updateWeather0(Weather0 weather0);

    void updateWeather1(Weather1 weather1);

    void updateWeather2(Weather2 weather2);

    void updateWeather3(Weather3 weather3);

    void updateWeather4(Weather4 weather4);

    void updateWeather5(Weather5 weather5);

    void updateWeather6(Weather6 weather6);

    void updateWeather7(Weather7 weather7);

    void updateHours(ArrayList<Hours> arrayList);
}
