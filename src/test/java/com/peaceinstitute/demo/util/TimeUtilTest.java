package com.peaceinstitute.demo.util;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Describe:
 *
 * @author JZY
 */
public class TimeUtilTest {

    @Test
    public void getNowMinute() {
    }

    @Test
    public void getNowHour() {
    }

    @Test
    public void getTimeGap() {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse("2020-05-01 16:00:00");

            System.out.println(TimeUtil.getTimeGap(date, 1000 * 60 * 50));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}