package com.peaceinstitute.demo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.peaceinstitute.demo.dao.dataSource1.IInitializeMapper;
import com.peaceinstitute.demo.pojo.*;
import com.peaceinstitute.demo.service.weatherService.ITimingTaskService;
import com.peaceinstitute.demo.service.weatherService.IUpdateWeatherService;
import com.peaceinstitute.demo.util.GetIPUtil;
import com.peaceinstitute.demo.util.SendMailUtil;
import com.peaceinstitute.demo.util.WeatherUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import java.io.*;
import java.net.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Describe:
 *
 * @author JZY
 */

@SpringBootTest
public class JZYTest {

    @Autowired
    IInitializeMapper iInitializeMapper;
    @Autowired
    Initialize initialize;
    @Autowired
    ITimingTaskService iTimingTaskService;
    @Autowired
    IUpdateWeatherService iUpdateWeatherService;
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    RedisTemplate redisTemplate;  //k-v都是对象的

    @Test
    public void InitializeCityInformation(){

        System.out.println(initialize.toString());

//        利用自定义工具类处理
        HashMap<String,String> map = WeatherUtil.patternUtil(initialize.getCityInformation());

        System.out.println(map);
        int i = 0;
        for(Map.Entry<String,String> entry : map.entrySet()){
//            将获取好的key-value，存入到数据库
            iInitializeMapper.initializeCityInfo(entry.getKey(),entry.getValue(),"nothing");
            iInitializeMapper.initializeWeather0(entry.getValue());
            iInitializeMapper.initializeWeather1(entry.getValue());
            iInitializeMapper.initializeWeather2(entry.getValue());
            iInitializeMapper.initializeWeather3(entry.getValue());
            iInitializeMapper.initializeWeather4(entry.getValue());
            iInitializeMapper.initializeWeather5(entry.getValue());
            iInitializeMapper.initializeWeather6(entry.getValue());
            iInitializeMapper.initializeWeather7(entry.getValue());
            iInitializeMapper.initializeHour08(entry.getValue());
            iInitializeMapper.initializeHour11(entry.getValue());
            iInitializeMapper.initializeHour14(entry.getValue());
            iInitializeMapper.initializeHour17(entry.getValue());
            iInitializeMapper.initializeHour20(entry.getValue());
            iInitializeMapper.initializeHour23(entry.getValue());
            System.out.println(entry.getKey() + ":" + entry.getValue());
            System.out.println(++i);
        }
    }

    @Test
    public void testWeather() throws IOException {

        HashMap<String,String> map = WeatherUtil.patternUtil(initialize.getCityInformation());

        String cityKey = map.get("武汉");
        System.out.println("获取到的cityKey：" + cityKey);
        String apiUrl = "http://t.weatherService.sojson.com/api/weatherService/city/".concat(cityKey);
        System.out.println(apiUrl);

        //开始请求
        URL url= new URL(apiUrl);
        URLConnection open = url.openConnection();
        InputStream input = open.getInputStream();

        //这里转换为String，带上包名，避免引错包
        String information = org.apache.commons.io.IOUtils.toString(input,"utf-8");
        //输出
        System.out.println(information);


        JSONObject information1 = JSONObject.parseObject(information);

        JSONObject information2_cityInfo = information1.getJSONObject("cityInfo");
        JSONObject information2_data = information1.getJSONObject("data");

        JSONObject information3_yesterday = information2_data.getJSONObject("yesterday");
        JSONArray information3_forecast = information2_data.getJSONArray("forecast");


        System.out.println(information1);
        System.out.println(information2_cityInfo);
        System.out.println(information2_data);
        System.out.println(information3_yesterday);
        System.out.println(information3_forecast);
        System.out.println(information3_forecast.get(1));
    }

    @Test
    public void testITimingTask() throws IOException, InterruptedException, ParseException {

        try {
            iTimingTaskService.updateAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSendMail() throws MessagingException {

        try {
            SendMailUtil.sendQQMail(javaMailSender, new Exception("123456789"), -1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetCityKey(){

        System.out.println(WeatherUtil.getCityKey("运城", initialize.getCityInformation()));
        System.out.println(WeatherUtil.getCityKey("南昌市", initialize.getCityInformation()));
        System.out.println(WeatherUtil.getCityKey("南昌镇", initialize.getCityInformation()));
        System.out.println(WeatherUtil.getCityKey("南昌县", initialize.getCityInformation()));
        System.out.println(WeatherUtil.getCityKey("南昌区", initialize.getCityInformation()));
        System.out.println(WeatherUtil.getCityKey("南昌村", initialize.getCityInformation()));
    }

    @Test
    public void testCompareTime() throws ParseException {

        Weather0 weather0 = (Weather0) redisTemplate.opsForValue().get("weather0::101240101");
        Weather1 weather1 = (Weather1) redisTemplate.opsForValue().get("weather1::101240101");
        Weather2 weather2 = (Weather2) redisTemplate.opsForValue().get("weather2::101240101");
        Weather3 weather3 = (Weather3) redisTemplate.opsForValue().get("weather3::101240101");
        Weather4 weather4 = (Weather4) redisTemplate.opsForValue().get("weather4::101240101");
        Weather5 weather5 = (Weather5) redisTemplate.opsForValue().get("weather5::101240101");
        Weather6 weather6 = (Weather6) redisTemplate.opsForValue().get("weather6::101240101");
        Weather7 weather7 = (Weather7) redisTemplate.opsForValue().get("weather7::101240101");

        System.out.println(weather0.getClass());
        System.out.println(weather0.getClass().getName());

        ArrayList<Object> arrayList = new ArrayList<>();
        arrayList.add(new CityInfo());
        arrayList.add(weather0);
        arrayList.add(weather1);
        arrayList.add(weather2);
        arrayList.add(weather3);
        arrayList.add(weather4);
        arrayList.add(weather5);
        arrayList.add(weather6);
        arrayList.add(weather7);

//        System.out.println(CompareTimeUtil.compareTime());
    }

    @Test
    public void testRealTimeInfo() throws IOException {

        String realTimeInfo = WeatherUtil.getRealTimeWeatherInformation("101200101");
        JSONObject information1 = JSONObject.parseObject(realTimeInfo);
        JSONArray information2 = information1.getJSONArray("data");
        JSONObject information3 = information2.getJSONObject(0);
        JSONArray information4 = information3.getJSONArray("hours");

        System.out.println(information1);
        System.out.println(information2);
        System.out.println(information3);
        System.out.println(information4);
        System.out.println(information4.get(0));
        System.out.println(information4.get(2));
    }

    @Test
    public void testGetHour() {

        Calendar calendar = Calendar.getInstance();

        int hour12 = calendar.get(calendar.HOUR);
        int hour24 = calendar.get(calendar.HOUR_OF_DAY);


        System.out.println(hour12);
        System.out.println(hour24);
    }

    @Test
    public void testGetIP() {

        try {
            System.out.println(GetIPUtil.getIntranetIP());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(GetIPUtil.getExtranetIP());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Test
//    public  void testUpdateWeather() throws IOException, ParseException {
//        String string = "{\"message\":\"success感谢又拍云(upyun.com)提供CDN赞助\",\"status\":200,\"date\":\"20200206\",\"time\":\"2020-02-06 23:37:05\",\"cityInfo\":{\"city\":\"南昌市\",\"citykey\":\"101240101\",\"parent\":\"江西\",\"updateTime\":\"22:46\"},\"data\":{\"shidu\":\"94%\",\"pm25\":16.0,\"pm10\":14.0,\"quality\":\"优\",\"wendu\":\"5\",\"ganmao\":\"各类人群可自由活动\",\"forecast\":[{\"date\":\"06\",\"high\":\"高温 9℃\",\"low\":\"低温 6℃\",\"ymd\":\"2020-02-06\",\"week\":\"星期四\",\"sunrise\":\"07:03\",\"sunset\":\"17:59\",\"aqi\":24,\"fx\":\"东北风\",\"fl\":\"3-4级\",\"type\":\"中雨\",\"notice\":\"记得随身携带雨伞哦\"},{\"date\":\"07\",\"high\":\"高温 10℃\",\"low\":\"低温 6℃\",\"ymd\":\"2020-02-07\",\"week\":\"星期五\",\"sunrise\":\"07:03\",\"sunset\":\"18:00\",\"aqi\":168,\"fx\":\"无持续风向\",\"fl\":\"3-4级\",\"type\":\"阴\",\"notice\":\"不要被阴云遮挡住好心情\"},{\"date\":\"08\",\"high\":\"高温 12℃\",\"low\":\"低温 6℃\",\"ymd\":\"2020-02-08\",\"week\":\"星期六\",\"sunrise\":\"07:02\",\"sunset\":\"18:00\",\"aqi\":98,\"fx\":\"无持续风向\",\"fl\":\"3-4级\",\"type\":\"多云\",\"notice\":\"阴晴之间，谨防紫外线侵扰\"},{\"date\":\"09\",\"high\":\"高温 14℃\",\"low\":\"低温 5℃\",\"ymd\":\"2020-02-09\",\"week\":\"星期日\",\"sunrise\":\"07:01\",\"sunset\":\"18:01\",\"aqi\":57,\"fx\":\"北风\",\"fl\":\"<3级\",\"type\":\"多云\",\"notice\":\"阴晴之间，谨防紫外线侵扰\"},{\"date\":\"10\",\"high\":\"高温 16℃\",\"low\":\"低温 6℃\",\"ymd\":\"2020-02-10\",\"week\":\"星期一\",\"sunrise\":\"07:01\",\"sunset\":\"18:02\",\"aqi\":56,\"fx\":\"南风\",\"fl\":\"<3级\",\"type\":\"多云\",\"notice\":\"阴晴之间，谨防紫外线侵扰\"},{\"date\":\"11\",\"high\":\"高温 18℃\",\"low\":\"低温 10℃\",\"ymd\":\"2020-02-11\",\"week\":\"星期二\",\"sunrise\":\"07:00\",\"sunset\":\"18:03\",\"aqi\":43,\"fx\":\"西南风\",\"fl\":\"<3级\",\"type\":\"小雨\",\"notice\":\"雨虽小，注意保暖别感冒\"},{\"date\":\"12\",\"high\":\"高温 17℃\",\"low\":\"低温 12℃\",\"ymd\":\"2020-02-12\",\"week\":\"星期三\",\"sunrise\":\"06:59\",\"sunset\":\"18:03\",\"fx\":\"东北风\",\"fl\":\"<3级\",\"type\":\"中雨\",\"notice\":\"记得随身携带雨伞哦\"},{\"date\":\"13\",\"high\":\"高温 18℃\",\"low\":\"低温 12℃\",\"ymd\":\"2020-02-13\",\"week\":\"星期四\",\"sunrise\":\"06:59\",\"sunset\":\"18:04\",\"fx\":\"东南风\",\"fl\":\"<3级\",\"type\":\"小雨\",\"notice\":\"雨虽小，注意保暖别感冒\"},{\"date\":\"14\",\"high\":\"高温 18℃\",\"low\":\"低温 12℃\",\"ymd\":\"2020-02-14\",\"week\":\"星期五\",\"sunrise\":\"06:58\",\"sunset\":\"18:05\",\"fx\":\"北风\",\"fl\":\"<3级\",\"type\":\"阴\",\"notice\":\"不要被阴云遮挡住好心情\"},{\"date\":\"15\",\"high\":\"高温 17℃\",\"low\":\"低温 9℃\",\"ymd\":\"2020-02-15\",\"week\":\"星期六\",\"sunrise\":\"06:57\",\"sunset\":\"18:06\",\"fx\":\"东北风\",\"fl\":\"3-4级\",\"type\":\"多云\",\"notice\":\"阴晴之间，谨防紫外线侵扰\"},{\"date\":\"16\",\"high\":\"高温 10℃\",\"low\":\"低温 3℃\",\"ymd\":\"2020-02-16\",\"week\":\"星期日\",\"sunrise\":\"06:56\",\"sunset\":\"18:07\",\"fx\":\"北风\",\"fl\":\"3-4级\",\"type\":\"小雨\",\"notice\":\"雨虽小，注意保暖别感冒\"},{\"date\":\"17\",\"high\":\"高温 9℃\",\"low\":\"低温 3℃\",\"ymd\":\"2020-02-17\",\"week\":\"星期一\",\"sunrise\":\"06:55\",\"sunset\":\"18:07\",\"fx\":\"东北风\",\"fl\":\"<3级\",\"type\":\"多云\",\"notice\":\"阴晴之间，谨防紫外线侵扰\"},{\"date\":\"18\",\"high\":\"高温 11℃\",\"low\":\"低温 6℃\",\"ymd\":\"2020-02-18\",\"week\":\"星期二\",\"sunrise\":\"06:54\",\"sunset\":\"18:08\",\"fx\":\"东北风\",\"fl\":\"<3级\",\"type\":\"小雨\",\"notice\":\"雨虽小，注意保暖别感冒\"},{\"date\":\"19\",\"high\":\"高温 14℃\",\"low\":\"低温 8℃\",\"ymd\":\"2020-02-19\",\"week\":\"星期三\",\"sunrise\":\"06:54\",\"sunset\":\"18:09\",\"fx\":\"东北风\",\"fl\":\"<3级\",\"type\":\"阴\",\"notice\":\"不要被阴云遮挡住好心情\"},{\"date\":\"20\",\"high\":\"高温 19℃\",\"low\":\"低温 12℃\",\"ymd\":\"2020-02-20\",\"week\":\"星期四\",\"sunrise\":\"06:53\",\"sunset\":\"18:09\",\"fx\":\"东北风\",\"fl\":\"<3级\",\"type\":\"小雨\",\"notice\":\"雨虽小，注意保暖别感冒\"}],\"yesterday\":{\"date\":\"05\",\"high\":\"高温 16℃\",\"low\":\"低温 8℃\",\"ymd\":\"2020-02-05\",\"week\":\"星期三\",\"sunrise\":\"07:04\",\"sunset\":\"17:58\",\"aqi\":49,\"fx\":\"东北风\",\"fl\":\"3-4级\",\"type\":\"多云\",\"notice\":\"阴晴之间，谨防紫外线侵扰\"}}}";
//        System.out.println(string);
//
//        string = WeatherUtil.getWeatherInformation("101240101");
//        System.out.println(string);
//
//        JSONObject information1 = JSONObject.parseObject(string);
//
//        JSONObject information2_cityInfo = information1.getJSONObject("cityInfo");
//        JSONObject information2_data = information1.getJSONObject("data");
//
//        JSONObject information3_yesterday = information2_data.getJSONObject("yesterday");
//        JSONArray information3_forecast = information2_data.getJSONArray("forecast");
//
//        JSONObject information3_forecast1 = information3_forecast.getJSONObject(0);
//        JSONObject information3_forecast2 = information3_forecast.getJSONObject(1);
//        JSONObject information3_forecast3 = information3_forecast.getJSONObject(2);
//        JSONObject information3_forecast4 = information3_forecast.getJSONObject(3);
//        JSONObject information3_forecast5 = information3_forecast.getJSONObject(4);
//        JSONObject information3_forecast6 = information3_forecast.getJSONObject(5);
//        JSONObject information3_forecast7 = information3_forecast.getJSONObject(6);
//
//        System.out.println(information1);
//        System.out.println(information2_cityInfo);
//        System.out.println(information2_data);
//        System.out.println(information3_yesterday);
//        System.out.println(information3_forecast);
//        System.out.println(information3_forecast.get(1));
//        System.out.println(information3_forecast1);
//        System.out.println(information3_forecast2);
//        System.out.println(information3_forecast3);
//        System.out.println(information3_forecast4);
//        System.out.println(information3_forecast5);
//        System.out.println(information3_forecast6);
//        System.out.println(information3_forecast7);
//
//        iUpdateWeatherService.updateCityInfo((String)information2_cityInfo.get("citykey"),
//                (String)information2_cityInfo.get("parent"), (String)information1.get("time"));
//
//        iUpdateWeatherService.updateWeather0((String)information2_cityInfo.get("citykey"),
//                (String)information3_yesterday.get("high"), (String)information3_yesterday.get("low"),
//                information3_yesterday.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_yesterday.get("week"), (String)information3_yesterday.get("sunrise"),
//                (String)information3_yesterday.get("sunset"), String.valueOf(information3_yesterday.get("aqi")),
//                (String)information3_yesterday.get("fx"), (String)information3_yesterday.get("fl"),
//                (String)information3_yesterday.get("type"), (String)information3_yesterday.get("notice"));
//
//        iUpdateWeatherService.updateWeather1((String)information2_cityInfo.get("citykey"),
//                (String)information2_data.get("shidu"), String.valueOf(information2_data.get("pm25")),
//                String.valueOf(information2_data.get("pm10")), (String)information2_data.get("quality"), (String)information2_data.get("ganmao"),
//                (String)information3_forecast1.get("high"), (String)information3_forecast1.get("low"),
//                information3_forecast1.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_forecast1.get("week"), (String)information3_forecast1.get("sunrise"),
//                (String)information3_forecast1.get("sunset"), String.valueOf(information3_forecast1.get("aqi")),
//                (String)information3_forecast1.get("fx"), (String)information3_forecast1.get("fl"),
//                (String)information3_forecast1.get("type"), (String)information3_forecast1.get("notice"));
//
//        iUpdateWeatherService.updateWeather2((String)information2_cityInfo.get("citykey"),
//                (String)information3_forecast2.get("high"), (String)information3_forecast2.get("low"),
//                information3_forecast2.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_forecast2.get("week"), (String)information3_forecast2.get("sunrise"),
//                (String)information3_forecast2.get("sunset"), String.valueOf(information3_forecast2.get("aqi")),
//                (String)information3_forecast2.get("fx"), (String)information3_forecast2.get("fl"),
//                (String)information3_forecast2.get("type"), (String)information3_forecast2.get("notice"));
//
//        iUpdateWeatherService.updateWeather3((String)information2_cityInfo.get("citykey"),
//                (String)information3_forecast3.get("high"), (String)information3_forecast2.get("low"),
//                information3_forecast3.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_forecast3.get("week"), (String)information3_forecast3.get("sunrise"),
//                (String)information3_forecast3.get("sunset"), String.valueOf(information3_forecast2.get("aqi")),
//                (String)information3_forecast3.get("fx"), (String)information3_forecast3.get("fl"),
//                (String)information3_forecast3.get("type"), (String)information3_forecast3.get("notice"));
//
//        iUpdateWeatherService.updateWeather4((String)information2_cityInfo.get("citykey"),
//                (String)information3_forecast4.get("high"), (String)information3_forecast4.get("low"),
//                information3_forecast4.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_forecast4.get("week"), (String)information3_forecast4.get("sunrise"),
//                (String)information3_forecast4.get("sunset"), String.valueOf(information3_forecast4.get("aqi")),
//                (String)information3_forecast4.get("fx"), (String)information3_forecast4.get("fl"),
//                (String)information3_forecast4.get("type"), (String)information3_forecast4.get("notice"));
//
//        iUpdateWeatherService.updateWeather5((String)information2_cityInfo.get("citykey"),
//                (String)information3_forecast5.get("high"), (String)information3_forecast5.get("low"),
//                information3_forecast5.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_forecast5.get("week"), (String)information3_forecast5.get("sunrise"),
//                (String)information3_forecast5.get("sunset"), String.valueOf(information3_forecast5.get("aqi")),
//                (String)information3_forecast5.get("fx"), (String)information3_forecast5.get("fl"),
//                (String)information3_forecast5.get("type"), (String)information3_forecast5.get("notice"));
//
//        iUpdateWeatherService.updateWeather6((String)information2_cityInfo.get("citykey"),
//                (String)information3_forecast6.get("high"), (String)information3_forecast6.get("low"),
//                information3_forecast6.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_forecast6.get("week"), (String)information3_forecast6.get("sunrise"),
//                (String)information3_forecast6.get("sunset"), String.valueOf(information3_forecast6.get("aqi")),
//                (String)information3_forecast6.get("fx"), (String)information3_forecast6.get("fl"),
//                (String)information3_forecast6.get("type"), (String)information3_forecast6.get("notice"));
//
//        iUpdateWeatherService.updateWeather7((String)information2_cityInfo.get("citykey"),
//                (String)information3_forecast7.get("high"), (String)information3_forecast7.get("low"),
//                information3_forecast7.get("ymd") + " " + information2_cityInfo.get("updateTime"),
//                (String)information3_forecast7.get("week"), (String)information3_forecast7.get("sunrise"),
//                (String)information3_forecast7.get("sunset"), String.valueOf(information3_forecast7.get("aqi")),
//                (String)information3_forecast7.get("fx"), (String)information3_forecast7.get("fl"),
//                (String)information3_forecast7.get("type"), (String)information3_forecast7.get("notice"));
//    }
}
